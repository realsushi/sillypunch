---
title: xcode's Simulator issue on iOS 16
category: mobile
tags: [ios, mobile, dev, apple, macbook, flutter, android-studio, nposix, crash, segfault, beta]
date: "2022-07-21 12:00:01"
---

# xCode VM crashes
as reported by [reddit]](https://www.reddit.com/r/Xcode/comments/wcd9wc/xcode_14_beta_4_simulator_crashing_posterboard/) xcode is not able to launch the simulator on some beta versions of xcode.

![crash](https://preview.redd.it/3b2vok5c8te91.png?width=526&format=png&auto=webp&s=cd3b06ebba86c072a3ec9d34a57437bbda4480da)

This sounds like a segfault, depending if reporting crash reports to apple is on or off it will show the error in detail or no error at all. Code: NPOSIX

## Workarounds:
- Add simulator and then download iOS version 15.5 (the next one doesn't exist!), then you need to fiddle with User Interface of Simulator/Environnment to select the aforementioned version. NB it takes 6GB of free space, what a cheap workaround.

or

- Don't use Simulator at all and use device direclty connected with USB.

## Impacts:
Because iOS simulator is used for general development, it will impact android-studio as well.

## How to install xCode beta
See other article

## How to install xCode without Apple Store or iCloud account
See other article


