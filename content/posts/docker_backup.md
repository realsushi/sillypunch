---
title: Docker tricks backup / import
category: tricks
tags: [docker, backup, import]
date: "2022-11-25 12:00:01"
---

# backup docker
most practical way to save stuff
## export process
```s
docker commit
docker export 265e1f9c08a0 > /tmp/poll.tar
```
## import method
```s
scp poll.tar
docker import /tmp/poll.tar 
```

*REM:* check if data timestamp to keep the latest version.