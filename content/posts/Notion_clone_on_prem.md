---
title: Notion clone but open source
category: design
tags: [ui, office, open-source, SaaS]
date: "2022-07-10 12:00:01"
---

# Notion has been cloned somehow
The main concern with SaaS is you don't own the data. This clone works on any device, on cloud or on a private computer.
Data is not lost, data is not stolen.

## Appflowy

https://appflowy.gitbook.io/docs/essential-documentation/readme