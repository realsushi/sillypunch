---
title: Chromebook for devs
category: Linux
tags: [computer, os, kernel, linux, space-bar]
date: "2022-10-10 12:00:01"
---

# Chromebook fancy stuff
Chromebook are generally part of that subset of computers, dirt cheap but low tier.
One pros though, some of them handle touch quite well.

## Native
### Native access and performance
Native experience comes with Android app support. But display is often broken.
pressing ctrl alt t enable crosh "chromebook shell" but it is rather uninteresting, so enter shell.
then a package library can be used with crew, the equivalent of brew in ruby.
### Native dev mode
Get access to more possibilities but still executing random binary is unlikely

### Holy grail of chromebook for a decade: crouton the chroot

Allow to run games, often the driver for sound is not loaded and needs a passthrough.
Can add IDE.

### User land
Did crash on my system.

## The official google solution: Crostini VM
### Crostini
is a vm within chromebook since few years now. It should provide Linux context quickly and bring capability to run other apps (the huge open-source catalog)
One detail is that default LXC is not a full Linux Kernel.
## About true Linux Kernel
Without access to a standard or custom kernel, some modules are not available.
example of feature needing kernel modules available for activation https://developer.ridgerun.com/wiki/index.php?title=How_to_setup_and_use_USB/IP
## Custom LXC
https://chromium.googlesource.com/chromiumos/docs/+/8c8ac04aed5d45bb6a14605c422dbbd01eeadf15/containers_and_vms.md

## Advanced features

### flags
flags are like registry but simpler
#### disabled stuff
- hyperthreading (+30/50% perfs)
- gpu support in crostini

### hardware
the hardware of chromebook is often a limiting feature. First come storage, which can be mitigated by usb ssd. Then comes the CPU. This is rather blocking for serious apps. Then Memory for low end spectrum. One surprising feature can be screen as it might have IPS and good brightness. Also there is arm or x86 cpu architecture. Arm for better mobility, x86 for more power in general.

### Flash the bootloader
possible but since CR50 devices it is a bit annoying to do and sound or other cpu features might not work. Drivers are not necessary standards and need custom tweaks. Linux experience has issue with kvm, some crashes on linux.


### VM stuff
https://www.liquidweb.com/kb/what-is-kernel-based-virtual-machine/